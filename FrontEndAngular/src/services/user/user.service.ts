import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { BehaviorSubject, Observable } from "rxjs";
import { of } from "rxjs/observable/of";
import { Login } from "../../interfaces/login.interface";
import { User } from "src/interfaces/user.interface";
import { UrlsEnum } from "src/model/enum/UrlsEnum";

@Injectable({
   providedIn: "root"
})
export class UserService {
   authenticationState: BehaviorSubject<boolean>;
   private currentUserSubject: BehaviorSubject<User>;

   constructor(public http: HttpClient) {
      this.authenticationState = new BehaviorSubject(false);
      // this.currentUserSubject = new BehaviorSubject<User>(this.base.getItem());
      // this.currentUser = this.currentUserSubject.asObservable();
   }

   /**
    * User authentication
    * @param user
    */
   userAuthentication(user: Login) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
     let usernameKey = user.username;
     
      return this.http
         .post<any>(UrlsEnum.LOGIN_URL.url, user, { headers: headers })
         .pipe(
            map(user => {

              if (user != null && user.responseBody.token != null) {
                localStorage.setItem("access-token", JSON.stringify(user.responseBody.token));
                localStorage.setItem('actual-user', (usernameKey));
                this.authenticationState.next(true);
              }

               return user;
            }),
            catchError(this.handleError<any>("userAuthentication"))
         );
   }

   /**
    * User registration
    * @param user
    */
   registerUser(user: Login) {
      const headers = new HttpHeaders({ "Content-Type": "application/json" });
      return this.http
         .post(UrlsEnum.REGISTER_URL.url, user, { headers: headers })
         .pipe(
            map(data => {
               return data;
            }),
            catchError(this.handleError<any>("registerUser"))
         );
   }

   private handleError<T>(operation = "operation", result?: T) {
      return (error: any): Observable<T> => {
         console.error(error);
         console.log(`${operation} failed: ${error.message}`);
         return of(error.error as T);
      };
   }

   isAuthenticated() {
      return this.authenticationState.value;
   }
}

/*import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import "rxjs/add/operator/map";
import { User } from "./user.model";
import { map, catchError } from "rxjs/operators";
import { Login } from "src/interfaces/login.interface";
import { Observable, of } from 'rxjs';

@Injectable()
export class UserService {
  readonly rootUrl = "http://localhost:8080/tecso";
  constructor(private http: HttpClient) {}


  userAuthentication(user:Login) {

    let reqHeader = new HttpHeaders({ 'Content-Type': 'application/json' });

console.log(this.rootUrl + "/users/login");

    return this.http
      .post<any>(this.rootUrl + "/users/login", user, { headers: reqHeader })
      .pipe(
        map(user => {
          if (user != null && user.responseBody.token != null) {
            localStorage.setItem(
              "userToken",
              JSON.stringify(user.responseBody.token)
            );
          }

          return user;
        }),
        catchError(this.handleError<any>("userAuthentication"))
      );
  }

  registerUser(user: User) {
    console.log('registerUser',user)
    const body: User = {
      username: user.username,
      password: user.password,
      email: user.email
    };

    console.log(this.rootUrl + "/users/register");

    let reqHeader = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.rootUrl + "/users/register", body, {
      headers: reqHeader
    }).pipe(map(data => {
      return data;
  }),
  catchError(this.handleError<any>('registerUser')));

  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      throw new Error("Method not implemented." + error);
    };
  }

  handleError1<T>(
    arg0: string
  ): (err: any, caught: import("rxjs").Observable<any>) => never {
    throw new Error("Method not implemented.");
  }

  getUserClaims() {
    return this.http.get(this.rootUrl + "/api/GetUserClaims");
  }
}
*/
