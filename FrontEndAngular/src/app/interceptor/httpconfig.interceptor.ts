import { Injectable } from '@angular/core';

import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UrlsEnum } from 'src/model/enum/UrlsEnum';

@Injectable()
export class HttpConfigInterceptor {  
   
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const token: string = JSON.parse(localStorage.getItem(UrlsEnum.TOKEN.name));
        if (!request.url.match("users")) {
            request = request.clone({ headers: request.headers.set('Token', token) });
            request = request.clone({ headers: request.headers.set('User', localStorage.getItem('actual-user')) });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }

        request = request.clone({ headers: request.headers.append('Accept', 'application/json') });
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                }
                return event;
            }));
  }

}
