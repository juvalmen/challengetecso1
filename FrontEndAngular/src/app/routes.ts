import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// import { AuthGuardService } from "../security/auth-guard.service";
import { UserComponent } from "./user/user.component";
import { SignUpComponent } from "./user/sign-up/sign-up.component";
import { SignInComponent } from "./user/sign-in/sign-in.component";
import { AuthGuard } from "./auth/auth.guard";
import { ContainerHomePersonaComponent } from "./container-home-persona/container-home-persona.component";

const routes: Routes = [
   { path: "", redirectTo: "/login", pathMatch: "full" }, // esto va

   //  { path: "", component: ContainerHomePersonaComponent}, // esto no va

   {
      path: "persona",
      component: ContainerHomePersonaComponent,
      canActivate: [AuthGuard] // esto va
   },
   {
      path: "signup",
      component: UserComponent,
      children: [{ path: "", component: SignUpComponent }]
   },
   {
      path: "login",
      component: UserComponent,
      children: [{ path: "", component: SignInComponent }]
   }
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
})
export class AppRoutingModule {}
