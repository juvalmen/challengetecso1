import { Component, OnInit, ViewChild } from "@angular/core";
import { MessageService, ConfirmationService } from "primeng/api";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { first } from "rxjs/operators";

import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Categoria } from "src/interfaces/categoria.interface";
import { Persona } from "src/interfaces/persona.interface";
import { PersonaService } from "src/services/persona/persona.service";
import { InputMask } from "primeng/inputmask";
import { CategoriaEnum } from "src/model/enum/CategoriaEnum";

@Component({
   selector: "app-persona",
   templateUrl: "./persona.component.html",
   styleUrls: ["./persona.component.css"],
   providers: [MessageService, ConfirmationService]
})
export class PersonAdminComponent implements OnInit {
   @ViewChild("myInputMaskCcv") myInputMaskCcv: InputMask;

   flagFisica: boolean;
   flagJuridica: boolean;
   formPerson: FormGroup;
   persons: Persona[] = [];
   dataTableCols: any[];
   categoriaPersona: Categoria[] = [];
   attrCcv: any;
   categoriaDropDown: Categoria;

   constructor(
      public messageService: MessageService,
      private _fb: FormBuilder,
      private personService: PersonaService,
      private confirmationService: ConfirmationService
   ) {
      console.log("constructor");
      this.initForm();
      this.initComponents();
   }

   initForm() {
      this.formPerson = this._fb.group({
         idpersona: [null],
         cuit: [null, Validators.required],
         categoriapersona: [null, Validators.required],
         nombre: [null, Validators.maxLength(80)],
         apellido: [null, Validators.maxLength(250)],
         dni: [null, Validators.maxLength(45)],
         razonsocial: [null, Validators.maxLength(100)],
         aniofundacion: [null]
      });
   }

   ngOnInit() {
      this.dataTableCols = [
         { field: "nombre", header: "Nombre/Razón social" },
         { field: "cuit", header: "CUIT" },
         { field: "dni", header: "DNI" },
         { field: "aniofundacion", header: "Año de fundación" },
         { field: "descripcionCategoriaPersona", header: "Tipo" }
      ];
      this.buildItemsEnumInArray();
      this.getPersons();
   }

   async buildItemsEnumInArray() {
      this.categoriaPersona = await Object.keys(CategoriaEnum).map(
         item => CategoriaEnum[item]
      );
      console.log(this.categoriaPersona);
   }

   initComponents() {
      this.flagFisica = true;
      this.flagJuridica = true;
      this.attrCcv = { attributeValue: "" };
   }

   isInputMaskFilledCcv(event) {
      if (!this.myInputMaskCcv.filled) {
         this.attrCcv = { attributeValue: "" };
      }
   }

   addMessage(title: string, message: string, type: string) {
      this.clearMessage();
      this.messageService.add({
         severity: type,
         summary: title,
         detail: message
      });
   }

   clearMessage() {
      this.messageService.clear();
   }

   getPersons() {
      this.personService
         .getPersons()
         .pipe(first())
         .subscribe(
            data => {
               if (data != undefined) {
                  if (data.statusCode == "200") {
                     this.persons = data.responseBody;
                     for (const persona of this.persons) {
                        persona.nombre = persona.nombre != null ? persona.nombre : persona.razonsocial;
                     }
                  } else {
                     this.addMessage("Error", data.responseMessage, "error");
                  }
               } else {
                  this.addMessage(
                     "Error",
                     "Error interno en la aplicación. Contacte con el administrador.",
                     "error"
                  );
               }
            },
            error => {
               this.addMessage("Error", error, "error");
            }
         );
   }

   onChangeCategoria() {
      const valorCategoriaPersona = this.formPerson.value.categoriapersona
         .valor;
      if (valorCategoriaPersona == 1) {
         this.flagFisica = false;
         this.flagJuridica = true;
      } else if (valorCategoriaPersona == 2) {
         this.flagJuridica = false;
         this.flagFisica = true;
      } else {
         this.flagJuridica = true;
         this.flagFisica = true;
      }
   }

   validarAntesDeGrabar() {
      let cuit = this.formPerson.value.cuit;
      console.log("cuit", cuit);
      if (cuit != null && cuit.length > 45) {
         this.addMessage(
            "",
            "El tamaño del campo CUIT es de máximo 45 caracteres.",
            "warn"
         );
         return;
      }
      let valor = this.formPerson.value.categoriapersona.valor;
      if (valor == null) {
         this.addMessage("", "Se debe seleccionar el tipo de titular.", "warn");
         return;
      } else if (valor == 1) {
         // Titular físico
         let nombre = this.formPerson.value.nombre;
         if (nombre == null || nombre.length == 0) {
            this.addMessage("", "El campo nombre es obligatorio.", "warn");
            return;
         }
         let apellido = this.formPerson.value.apellido;
         if (apellido == null || apellido.length == 0) {
            this.addMessage("", "El campo apellido es obligatorio.", "warn");
            return;
         }
         let dni = this.formPerson.value.dni;
         if (dni == null || dni.length == 0) {
            this.addMessage("", "El campo dni es obligatorio.", "warn");
            return;
         }
      } else if (valor == 2) {
         // Titular jurídico
         let razonsocial = this.formPerson.value.razonsocial;
         if (razonsocial == null || razonsocial.length == 0) {
            this.addMessage("", "El campo razonsocial es obligatorio.", "warn");
            return;
         }
         let aniofundacion = this.formPerson.value.aniofundacion;
         if (aniofundacion == null || aniofundacion.length == 0) {
            this.addMessage(
               "",
               "El campo año de fundación es obligatorio.",
               "warn"
            );
            return;
         }
      }

      const person: Persona = {
         idpersona: this.formPerson.value.idpersona,
         cuit: this.formPerson.value.cuit,
         idCategoriaPersona: this.formPerson.value.categoriapersona.idcategoria,
         valorCategoriaPersona: this.formPerson.value.categoriapersona.valor
      };
      const valorCategoriaPersona = this.formPerson.value.categoriapersona.valor;
      if (valorCategoriaPersona == 1) {
         person.nombre = this.formPerson.value.nombre;
         person.apellido = this.formPerson.value.apellido;
         person.dni = this.formPerson.value.dni;
      } else if (valorCategoriaPersona == 2) {
         person.razonsocial = this.formPerson.value.razonsocial;
         person.aniofundacion = this.formPerson.value.aniofundacion;
      }

      // Save or update
      console.log("person", person);
      if (person.idpersona == null) {
         this.savePerson(person);
      } else {
         this.updatePerson(person);
      }
   }

   savePerson(person: Persona) {
      this.personService
         .savePerson(person)
         .pipe(first())
         .subscribe(
            data => {
               if (data != undefined && data != null) {
                  if (data.statusCode == "200") {
                     this.addMessage("", 'Se ha registrado el titular satisfactoriamente.', "info");
                    this.getPersons();
                    this.clearForm();
                  } else {
                     this.addMessage("", data.responseMessage, "warn");
                  }
               } else {
                  this.addMessage("", "Error interno en la aplicación. Contacte con el administrador.", "error");
               }
            },
            error => {
               this.addMessage(
                  "",
                  "Error interno en la aplicación. Contacte con el administrador." + error,
                  "error"
               );
            }
         );
   }

   updatePerson(person: Persona) {
      this.personService
         .updatePerson(person)
         .pipe(first())
         .subscribe(
            data => {
               if (data != undefined && data != null) {
                  if (data.statusCode == "200") {
                     this.addMessage("", 'Se ha actualizado el titular satisfactoriamente.', "info");
                    this.getPersons();
                    this.clearForm();
                  } else {
                     this.addMessage("", data.responseMessage, "warn");
                  }
               } else {
                  this.addMessage("", "Error interno en la aplicación. Contacte con el administrador.", "error");
               }
            },
            error => {
               this.addMessage(
                  "",
                  "Error interno en la aplicación. Contacte con el administrador." + error,
                  "error"
               );
            }
         );
   }

   deletePerson(person: Persona) {
      this.personService
         .deletePerson(person.idpersona)
         .pipe(first())
         .subscribe(
            data => {
               if (data != undefined && data != null) {
                  if (data.statusCode == "200") {
                     this.addMessage("", 'Se ha eliminado el titular satisfactoriamente.', "info");
                    this.getPersons();
                    this.clearForm();
                  } else {
                     this.addMessage("", data.responseMessage, "warn");
                  }
               } else {
                  this.addMessage("", "Error interno en la aplicación. Contacte con el administrador.", "error");
               }
            },
            error => {
               this.addMessage(
                  "",
                  "Error interno en la aplicación. Contacte con el administrador." + error,
                  "error"
               );
            }
         );
   }

   clearForm() {
      this.initComponents();

      this.formPerson = this._fb.group({
         idpersona: [null],
         cuit: [null, Validators.required],
         categoriapersona: [null, Validators.required],
         nombre: [null, Validators.maxLength(80)],
         apellido: [null, Validators.maxLength(250)],
         dni: [null, Validators.maxLength(45)],
         razonsocial: [null, Validators.maxLength(100)],
         aniofundacion: [null]
      });

   }
  
  deleteConfirm(person: Persona) {
    if (confirm('Desea eliminar este registro?')) {
      this.deletePerson(person);
    }
  }

   onEditPerson(person: Persona) {

      if (person.valorCategoriaPersona == "1") {
         this.flagFisica = false;
         this.flagJuridica = true;
      } else if (person.valorCategoriaPersona == "2") {
         this.flagJuridica = false;
         this.flagFisica = true;
      }

      let categoria: Categoria = {
         idcategoria: person.idCategoriaPersona,
         nombre: person.descripcionCategoriaPersona,
         valor: person.valorCategoriaPersona
      };

      this.categoriaDropDown = categoria;

      this.formPerson = this._fb.group({
         idpersona: [person.idpersona],
         cuit: [person.cuit, Validators.required],
         categoriapersona: [categoria.valor == '1' ? CategoriaEnum.FIS : CategoriaEnum.JUR, Validators.required],
         nombre: [person.nombre, Validators.maxLength(80)],
         apellido: [person.apellido, Validators.maxLength(250)],
         dni: [person.dni, Validators.maxLength(45)],
         razonsocial: [person.razonsocial, Validators.maxLength(100)],
         aniofundacion: [person.aniofundacion]
      });

   }
}
