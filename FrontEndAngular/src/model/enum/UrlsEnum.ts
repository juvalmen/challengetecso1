export const UrlsEnum = {
  ROOT_URL: {
    url: 'http://localhost:8080/tecso'    
  }, 
  // User
  LOGIN_URL: {
    url: 'http://localhost:8080/tecso/users/login'   
  },
  REGISTER_URL: {
    url: 'http://localhost:8080/tecso/users/register'    
  },
  // User

  // Persona
  SAVE_PERSONA_URL: {
    url: 'http://localhost:8080/tecso/persona/save'
  },
  UPDATE_PERSONA_URL: {
    url: 'http://localhost:8080/tecso/persona/update'
  },
  SEARCH_PERSONA_URL: {
    url: 'http://localhost:8080/tecso/persona/getAll'
  },
  DELETE_PERSONA_URL: {
    url: 'http://localhost:8080/tecso/persona/delete'
  },
  // Persona

  // Security
  TOKEN: {
    name: 'access-token'
  },
  // Security
}

