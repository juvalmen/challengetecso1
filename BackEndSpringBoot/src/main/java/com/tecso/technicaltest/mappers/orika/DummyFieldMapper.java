package com.tecso.technicaltest.mappers.orika;

import org.springframework.stereotype.Component;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.dto.UserLoginDto;
import com.tecso.technicaltest.model.Categoria;
import com.tecso.technicaltest.model.Persona;
import com.tecso.technicaltest.model.User;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
@Component("dummyFieldMapper")
public class DummyFieldMapper extends ConfigurableMapper {

    @Override
	public void configure(MapperFactory factory) {
    	
    	factory = new DefaultMapperFactory.Builder().build();
    	
    	factory.classMap(Categoria.class, CategoriaDto.class).register();
    	factory.classMap(User.class, UserLoginDto.class).byDefault().register();
    	factory.classMap(Persona.class, PersonaDto.class)
    	.field("categoria.idcategoria", "idCategoriaPersona")
    	.register();
    }
  

}

