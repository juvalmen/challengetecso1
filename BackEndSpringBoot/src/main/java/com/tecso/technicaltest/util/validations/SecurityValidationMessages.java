package com.tecso.technicaltest.util.validations;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
public class SecurityValidationMessages {
	
	public static final String INVALID_TOKEN = "El token no corresponde a un usuario activo.";
	public static final String USER_DOES_NOT_EXIST = "El usuario no se encuentra registrado.";
	public static final String USERNAME_EXIST = "El nombre de usuario ya se encuentra registrado en el sistema.";
	public static final String MAIL_EXIST = "El email ya se encuentra registrado en el sistema.";
	
}
