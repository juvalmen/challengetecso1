package com.tecso.technicaltest.util.validations;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
public class PersonaValidationMessages {
	
	// Persona física
	public static final String NOMBRE_NULO = "El nombre de la persona es obligatorio";
	public static final String NOMBRE_LONGITUD = "La longitud del nombre debe ser de máximo 80 caracteres";
	public static final String APELLIDO_NULO = "El apellido es obligatorio";
	public static final String APELLIDO_LONGITUD = "La longitud del apellido debe ser de máximo 250 caracteres";	
	public static final String CATEGORIA_PERSONA_NULO = "El tipo de titular es obligatorio";
	public static final String CATEGORIA_PERSONA_NO_EXISTE = "No se encontro el tipo de persona";
	
	// Persona jurídica
	public static final String ANIO_DE_FUNDACION_OBLIGATORIO = "El año de fundación es obligatorio";
	public static final String RAZON_SOCIAL_NULA = "La razón social es obligatoria";
	public static final String RAZON_SOCIAL_LONGITUD = "La longitud de la razón social debe ser de máximo 100 caracteres";
	public static final String DNI_NULO = "El DNI es obligatorio";
	public static final String DNI_LONGITUD = "La longitud del DNI debe ser de máximo 45 caracteres";
	
	public static final String ID_PERSONA_NO_EXISTE = "No se encontro el id del titular";
	public static final String CUIT_NULO = "El campo CUIT no puede ser nulo o vacio";
	public static final String CUIT_LONGITUD = "La longitud del cuit debe ser de máximo 45 caracteres";
	public static final String CUIT_EXISTE ="Ya existe un titular con el CUIT ingresado";
	
	// Categoria
	public static final String KEY_NO_ENCONTRADA = "No se encontro la llave de la categoría";
}
