package com.tecso.technicaltest.service;

import java.util.List;

import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

public interface PersonaService {
	List<PersonaDto> getPersonasList() throws TecsoCustomException;
	boolean savePersona(PersonaDto personDto) throws TecsoCustomException;
	boolean updatePersona(PersonaDto personDto) throws TecsoCustomException;
    boolean deletePersona(Long id) throws TecsoCustomException;
	PersonaDto getPersonaById(Long id) throws TecsoCustomException;
	boolean verificarCuit(String cuit) throws TecsoCustomException;
}
