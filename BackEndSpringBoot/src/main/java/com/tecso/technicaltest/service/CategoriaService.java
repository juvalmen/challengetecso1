package com.tecso.technicaltest.service;

import java.util.List;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

public interface CategoriaService {
	
	List<CategoriaDto> getCategoriaLlave(String name) throws TecsoCustomException;
	
	CategoriaDto getCategoriaPorId(Long id) throws TecsoCustomException;
	
}
