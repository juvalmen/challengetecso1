package com.tecso.technicaltest.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.enums.CategoriaEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Categoria;
import com.tecso.technicaltest.model.Persona;
import com.tecso.technicaltest.repository.jdbc.JdbcPersonaRepository;
import com.tecso.technicaltest.repository.jpa.JpaPersonaRepository;
import com.tecso.technicaltest.service.CategoriaService;
import com.tecso.technicaltest.service.PersonaService;
import com.tecso.technicaltest.util.validations.PersonaValidationMessages;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
public class PersonaServiceImpl implements PersonaService {

	private JpaPersonaRepository jpaPersonaRepository;
	private JdbcPersonaRepository jdbcPersonaRepository;
	private DummyFieldMapper dummyFieldMapper;
	private CategoriaService categoriaService;
	private static final Logger LOGGER = LogManager.getLogger(PersonaServiceImpl.class.getName());
	
	// Length fields
	private final int NAME_MAX_LENGTH = 80;
	private final int CUIT_MAX_LENGTH = 45;
	private final int DNI_MAX_LENGTH = 45;
	private final int LAST_NAME_MAX_LENGTH = 250;
	private final int SOCIAL_BUSINESS_MAX_LENGTH = 100;

	@Autowired
	public PersonaServiceImpl(JpaPersonaRepository jpaPersonaRepository, JdbcPersonaRepository jdbcPersonaRepository,
			DummyFieldMapper dummyFieldMapper,CategoriaService categoriaService) {
		this.jpaPersonaRepository = jpaPersonaRepository;
		this.jdbcPersonaRepository = jdbcPersonaRepository;
		this.dummyFieldMapper = dummyFieldMapper;
		this.categoriaService = categoriaService;
	}
	
	/**
	 * 
	 * @param personaDto
	 * @throws TecsoCustomException
	 */
	private void personaFisicaValidaciones(PersonaDto personaDto) throws TecsoCustomException {
		if (Strings.isNullOrEmpty(personaDto.getNombre())) {
			throw new TecsoCustomException(PersonaValidationMessages.NOMBRE_NULO);
		}
		if (personaDto.getNombre().length() > NAME_MAX_LENGTH) {
			throw new TecsoCustomException(PersonaValidationMessages.NOMBRE_LONGITUD);
		}
		if (Strings.isNullOrEmpty(personaDto.getApellido())) {
			throw new TecsoCustomException(PersonaValidationMessages.APELLIDO_NULO);
		}
		if (personaDto.getApellido().length() > LAST_NAME_MAX_LENGTH) {
			throw new TecsoCustomException(PersonaValidationMessages.APELLIDO_LONGITUD);
		}
		if (Strings.isNullOrEmpty(personaDto.getDni())) {
			throw new TecsoCustomException(PersonaValidationMessages.DNI_NULO);
		}
		if (personaDto.getDni().length() > DNI_MAX_LENGTH) {
			throw new TecsoCustomException(PersonaValidationMessages.DNI_LONGITUD);
		}
	}
	
	@Override
	public boolean savePersona(PersonaDto personaDto) throws TecsoCustomException {
		try {

			if (personaDto.getCuit() == null || personaDto.getCuit().isEmpty()) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_NULO);
			}
			
			if (personaDto.getCuit().length() > CUIT_MAX_LENGTH) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_LONGITUD);
			}
			
			if (personaDto.getIdCategoriaPersona() == null) {
				throw new TecsoCustomException(PersonaValidationMessages.CATEGORIA_PERSONA_NULO);
			}
			
			if (verificarCuit(personaDto.getCuit())) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_EXISTE);
			}

			CategoriaDto categoriaDto = categoriaService.getCategoriaPorId(personaDto.getIdCategoriaPersona());
			
			if (categoriaDto.getValor().equals(CategoriaEnum.FISICA.getType())) {
				personaFisicaValidaciones(personaDto);
			} else if (categoriaDto.getValor().equals(CategoriaEnum.JURIDICA.getType())) {
				personaJuridicaValidaciones(personaDto);
			}

			Categoria categoria = dummyFieldMapper.map(categoriaDto, Categoria.class);
			Persona person = dummyFieldMapper.map(personaDto, Persona.class);
			person.setCategoria(categoria);
			
			jpaPersonaRepository.save(person);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e.getMessage());
		}
		return true;
	}

	@Override
	public List<PersonaDto> getPersonasList() throws TecsoCustomException {		
		List<PersonaDto>  personas = jdbcPersonaRepository.getPersonasList();
		return personas;
	}

	/**
	 * 
	 * @param personaDto
	 * @throws TecsoCustomException
	 */
	private void personaJuridicaValidaciones(PersonaDto personaDto) throws TecsoCustomException {
		if (personaDto.getRazonsocial() == null || personaDto.getRazonsocial().isEmpty()) {
			throw new TecsoCustomException(PersonaValidationMessages.RAZON_SOCIAL_NULA);
		}
		if (personaDto.getRazonsocial().length() > SOCIAL_BUSINESS_MAX_LENGTH) {
			throw new TecsoCustomException(PersonaValidationMessages.RAZON_SOCIAL_LONGITUD);
		}
		if (personaDto.getAniofundacion() == null) {
			throw new TecsoCustomException(PersonaValidationMessages.ANIO_DE_FUNDACION_OBLIGATORIO);
		}
	}

	
	

	@Override
	public boolean updatePersona(PersonaDto personaDto) throws TecsoCustomException {
		try {

			if (personaDto.getIdpersona() == null) {
				throw new TecsoCustomException(PersonaValidationMessages.ID_PERSONA_NO_EXISTE);
			}

			if (Strings.isNullOrEmpty(personaDto.getCuit())) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_NULO);
			}
			
			if (personaDto.getCuit().length() > CUIT_MAX_LENGTH) {
				throw new TecsoCustomException(PersonaValidationMessages.CUIT_LONGITUD);
			}

			CategoriaDto categoriaDto = categoriaService.getCategoriaPorId(personaDto.getIdCategoriaPersona());
						
			PersonaDto personValidate = this.getPersonaById(personaDto.getIdpersona());
			if(personValidate == null || personValidate.getIdpersona() == null) {
				throw new TecsoCustomException(PersonaValidationMessages.ID_PERSONA_NO_EXISTE);
			}

			if (categoriaDto.getValor().equals(CategoriaEnum.FISICA.getType())) {
				personaFisicaValidaciones(personaDto);
			} else if (categoriaDto.getValor().equals(CategoriaEnum.JURIDICA.getType())) {
				personaJuridicaValidaciones(personaDto);
			}			
			
			if(!personValidate.getCuit().equalsIgnoreCase(personaDto.getCuit()) && (verificarCuit(personaDto.getCuit()))) {
			   throw new TecsoCustomException(PersonaValidationMessages.CUIT_EXISTE);
			}

			Categoria categoria = dummyFieldMapper.map(categoriaDto, Categoria.class);
			Persona personaUdate = dummyFieldMapper.map(personaDto, Persona.class);
			personaUdate.setCategoria(categoria);
	
			jpaPersonaRepository.save(personaUdate);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e.getMessage());
		}
		return true;
	}

	@Override
	public boolean deletePersona(Long id) throws TecsoCustomException {
		try {
			jpaPersonaRepository.deleteById(id);
		} catch (Exception e) {
			throw new TecsoCustomException(e);
		}
		return true;
	}

	@Override
	public PersonaDto getPersonaById(Long id) throws TecsoCustomException {
		try {
			Optional<Persona> persona = jpaPersonaRepository.findById(id);
			if(persona.isPresent()) {
				return dummyFieldMapper.map(persona.get(), PersonaDto.class);
			}
			return null;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e);
		}
	}

	@Override
	public boolean verificarCuit(String cuit) throws TecsoCustomException {
		try {
			boolean cuitExist = false;
			List<PersonaDto> person = jdbcPersonaRepository.getPersonaPorIdentificacionCuit(cuit);
			if (person != null && !person.isEmpty()) {
				cuitExist = true;
			}
			return cuitExist;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e);
		}
	}

	

}
