package com.tecso.technicaltest.repository.jdbc;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.model.Categoria;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
@Repository
public class JdbcCategoriaRepository {

	private JdbcTemplate jdbcTemplate;
	EntityManager entityManager;

	@Autowired
	public JdbcCategoriaRepository(JdbcTemplate jdbcTemplate, EntityManager entityManager) {
		this.jdbcTemplate = jdbcTemplate;
		this.entityManager = entityManager;
	}

	/**
	 * 
	 * @return
	 * @throws EmptyResultDataAccessException
	 */
	public List<Categoria> getCategoriaEntityList() throws EmptyResultDataAccessException {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<Categoria> q = cb.createQuery(Categoria.class);
		Root<Categoria> c = q.from(Categoria.class);
		q.select(c);

		return entityManager.createQuery(q).getResultList();

	}

	/**
	 * 
	 * @return
	 * @throws EmptyResultDataAccessException
	 */
	public List<CategoriaDto> getCategoriasList() throws EmptyResultDataAccessException {
		return jdbcTemplate.query("SELECT idcategoria, nombre, valor FROM categoria cat", 
				new BeanPropertyRowMapper<CategoriaDto>(CategoriaDto.class));
	}

	/**
	 * 
	 * @param key
	 * @return
	 * @throws EmptyResultDataAccessException
	 */
	public List<Categoria> getCategoriaLlave(String key) throws EmptyResultDataAccessException {
		

			CriteriaBuilder cb = entityManager.getCriteriaBuilder();

			CriteriaQuery<Categoria> q = cb.createQuery(Categoria.class);
			Root<Categoria> c = q.from(Categoria.class);
			q.select(c).where(cb.equal(c.get("llave"), key));

			return entityManager.createQuery(q).getResultList();

	}

}
