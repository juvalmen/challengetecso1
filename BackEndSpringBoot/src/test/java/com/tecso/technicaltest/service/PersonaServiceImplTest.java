package com.tecso.technicaltest.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.enums.CategoriaEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Categoria;
import com.tecso.technicaltest.model.Persona;
import com.tecso.technicaltest.repository.jdbc.JdbcPersonaRepository;
import com.tecso.technicaltest.repository.jpa.JpaPersonaRepository;
import com.tecso.technicaltest.service.impl.CategoriaServiceImpl;
import com.tecso.technicaltest.service.impl.PersonaServiceImpl;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@RunWith(SpringRunner.class)
@SpringBootTest()
@Transactional
@ActiveProfiles("test") 
public class PersonaServiceImplTest {
	
	@Autowired
	@InjectMocks
	private PersonaServiceImpl personaServiceImpl;

	@Mock
	private JdbcPersonaRepository jdbcPersonaRepository;
	
	@Mock
	private JpaPersonaRepository jpaPersonaRepository;
	
	@Mock
	private CategoriaServiceImpl categoriaServiceImpl;
	
	@Mock
	private DummyFieldMapper dummyFieldMapper;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * 
	 */
	public PersonaDto getPersonaDto() {
		PersonaDto personaDto = new PersonaDto();
		personaDto.setNombre("Julian");
		personaDto.setApellido("Valencia");
		personaDto.setCuit("123456");
		personaDto.setDni("444444");
		personaDto.setRazonsocial("Arquitecto de Software");
		personaDto.setAniofundacion(1982);
		personaDto.setIdCategoriaPersona(1L);
		return personaDto;
	}
	
	/**
	 * 
	 */
	public Persona getPersona() {
		Persona persona = new Persona();
		persona.setNombre("Julian");
		persona.setApellido("Valencia");
		persona.setCuit("123456");
		persona.setDni("444444");
		persona.setRazonsocial("Arquitecto de Software");
		persona.setAniofundacion(1982);
		persona.setCategoria(getCategoria());
		return persona;
	}
	
	/**
	 * 
	 */
	public List<PersonaDto> getListPersonaDto() {
		List<PersonaDto> list = new ArrayList<PersonaDto>();
		list.add(getPersonaDto());
		return list;
	}
	
	/**
	 * 
	 */
	public CategoriaDto getCategoriaDto() {
		CategoriaDto categoriaDto = new CategoriaDto();
		categoriaDto.setIdcategoria(1L);
		categoriaDto.setNombre(CategoriaEnum.FISICA.getType());
		categoriaDto.setValor("1");
		categoriaDto.setLlave("categoriapersona");
		return categoriaDto;
	}
	
	/**
	 * 
	 */
	public Categoria getCategoria() {
		Categoria categoria = new Categoria();
		categoria.setIdcategoria(1L);
		categoria.setNombre(CategoriaEnum.FISICA.getType());
		categoria.setValor("1");
		categoria.setLlave("categoriapersona");
		return categoria;
	}
	
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestCuitNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setCuit(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestCuitEmpty() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setCuit("");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestCategoryNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestCuitExist() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(getListPersonaDto());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestNameNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setNombre(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestNameLentgh() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setNombre("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestLastNameNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setApellido(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestLastNameLentgh() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setApellido("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestCuitLentgh() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setCuit("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestDniNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setDni(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestDniLength() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setDni("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestRazonSocialNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(2L);
			categoriaDto.setValor("2");
			personaDto.setRazonsocial(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(2L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestRazonSocialLength() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(2L);
			categoriaDto.setValor("2");
			personaDto.setRazonsocial("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, dadsadsfdsfsdf sd sdf sdfsd ");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(2L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void savePersonaTestAnioFundacionNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(2L);
			categoriaDto.setValor("2");
			personaDto.setAniofundacion(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(2L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
    
    @Test
    public void savePersonaTest() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
							
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.savePersona(personaDto);
    }
        
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestCuitNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setCuit(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestCuitEmpty() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setCuit("");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestCategoryNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestCuitExist() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(getListPersonaDto());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestNameNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setNombre(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestNameLentgh() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setNombre("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestLastNameNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setApellido(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestLastNameLentgh() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setApellido("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestCuitLentgh() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setCuit("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestDniNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setDni(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestDniLength() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setDni("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(1L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestRazonSocialNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(2L);
			categoriaDto.setValor("2");
			personaDto.setRazonsocial(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(2L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestRazonSocialLength() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(2L);
			categoriaDto.setValor("2");
			personaDto.setRazonsocial("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, dadsadsfdsfsdf sd sdf sdfsd ");
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(2L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void updatePersonaTestAnioFundacionNull() throws TecsoCustomException {
			Persona persona = getPersona();
			PersonaDto personaDto = getPersonaDto();
			personaDto.setIdpersona(1L);
			CategoriaDto categoriaDto = getCategoriaDto();
			Categoria categoria = getCategoria();
			
			personaDto.setIdCategoriaPersona(2L);
			categoriaDto.setValor("2");
			personaDto.setAniofundacion(null);
					
			when(jdbcPersonaRepository.getPersonaPorIdentificacionCuit("123456")).thenReturn(new ArrayList<PersonaDto>());
			
			when(categoriaServiceImpl.getCategoriaPorId(2L)).thenReturn(categoriaDto);
			
			when(dummyFieldMapper.map(categoriaDto, Categoria.class)).thenReturn(categoria);

			when(dummyFieldMapper.map(personaDto, Persona.class)).thenReturn(persona);
			
			personaServiceImpl.updatePersona(personaDto);
    }
    
    @Test
    public void deletePersonaTest() throws TecsoCustomException {
						
			personaServiceImpl.deletePersona(1L);
			
			PersonaDto personaDto2 = personaServiceImpl.getPersonaById(1L);
	
			assertEquals(null, personaDto2);
    }
    
    @Test
    public void getEmptyListPersonTest() throws TecsoCustomException {
		
		List<PersonaDto> list = personaServiceImpl.getPersonasList();
		
		assertEquals(true, list.isEmpty());
}

}
